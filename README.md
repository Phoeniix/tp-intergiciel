# TP Intergiciel : Kafka

### Prerequisites

Install kafka

A la racine de votre installation Kafka :

Sous Windows :

Commande de création server zookeeper/kafka :

    "bin/windows/zookeeper-server-start.bat" config/zookeeper.properties
    "bin/windows/kafka-server-start.bat" config/server.properties 

Commande de création de Topic :

    "bin/windows/kafka-topics" --zookeeper localhost:2181 \ --create \ --replication-factor 1 \ --partitions 1 \ --topic topic1
    "bin/windows/kafka-topics" --zookeeper localhost:2181 \ --create \ --replication-factor 1 \ --partitions 1 \ --topic topic2
    "bin/windows/kafka-topics" --zookeeper localhost:2181 \ --create \ --replication-factor 1 \ --partitions 1 \ --topic topic3
    
Sous Linux : 

Commande de création server zookeeper/kafka :

    "bin/zookeeper-server-start" config/zookeeper.properties
    "bin/kafka-server-start" config/server.properties 

Commande de création de Topic :

    "bin/kafka-topics" --zookeeper localhost:2181 \ --create \ --replication-factor 1 \ --partitions 1 \ --topic topic1
    "bin/kafka-topics" --zookeeper localhost:2181 \ --create \ --replication-factor 1 \ --partitions 1 \ --topic topic2
    "bin/kafka-topics" --zookeeper localhost:2181 \ --create \ --replication-factor 1 \ --partitions 1 \ --topic topic3
    
### Run

Lancer KafkaApplication (main class) pour run l'application.

Vous pouvez écrire une commande lorsqu'il n'y a plus de logs. (Le texte indiquant "Veuillez entrez une commande" se perd dans les logs, il est difficile de le voir malheureusement)

"help" pour voir les commandes disponibles

En cas d'erreur, relancer l'application.

Les données seront toujours les mêmes à l'exception de la date de mise à jour. (qui est la seule variable mise à jour sur l'API COVID19)

