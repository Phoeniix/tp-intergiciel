package com.tpintergiciel.kafka.spring.entities;

import java.sql.Timestamp;
import java.util.Collection;

public class Response {

    private String Message;
    private Global Global;
    private Collection<Country> Countries;
    private Timestamp Date;

    public Response() {}

    public Response(String Message, Global Global, Collection<Country> Countries, Timestamp Date) {
        this.Message = Message;
        this.Global = Global;
        this.Countries = Countries;
        this.Date = Date;
    }

    @Override
    public String toString() {
        return "Response{" +
                "Message='" + Message + '\'' +
                ", Global=" + Global.toString() +
                ", Date=" + Date +
                '}';
    }

    public void countriesToString () {
        Countries.forEach(country -> System.out.println(country.toString()));
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public void setGlobal(Global Global) {
        this.Global = Global;
    }

    public void setCountries(Collection<Country> Countries) {
        this.Countries = Countries;
    }

    public void setDate(Timestamp Date) {
        this.Date = Date;
    }

    public Global getGlobal() {
        return this.Global;
    }

    public Collection<Country> getCountries() {
        return Countries;
    }

    public Timestamp getDate() { return this.Date; }
}
