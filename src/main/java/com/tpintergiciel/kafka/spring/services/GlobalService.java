package com.tpintergiciel.kafka.spring.services;


import com.tpintergiciel.kafka.spring.entities.Global;
import com.tpintergiciel.kafka.spring.repositories.CountryRepository;
import com.tpintergiciel.kafka.spring.repositories.GlobalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class GlobalService {

    @Autowired
    private GlobalRepository globalRepository;

    @Autowired
    private CountryRepository countryRepository;


    public Global getGlobal() {
        return globalRepository.getGlobalValues();
    }

    public Double averageConfirmed() {
        Double avgConfirmed = globalRepository.getTotalConfirmed();
        return avgConfirmed / 192;
    }

    public Double averageDeaths() {
        Double avgDeaths = globalRepository.getTotalDeaths();
        return avgDeaths / 192;
    }

    public double averageRecovered() {
        double avgRecovered = globalRepository.getTotalRecovered();
        return avgRecovered / 192;
    }

    @Transactional
    public int createGlobal(Global global) {
        return globalRepository.save(global).getId();
    }


}
