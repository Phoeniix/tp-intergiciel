package com.tpintergiciel.kafka.spring.services;

import com.tpintergiciel.kafka.spring.entities.Country;
import com.tpintergiciel.kafka.spring.repositories.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

@Service
public class CountryService {

    @Autowired
    private CountryRepository countryRepository;


    public Country getCountry(String countryName){
        return countryRepository.getCountryValues(countryName);
    }

    public HashMap<String, Float> getDeathPercent(String countryName){
        float result;
        HashMap<String, Float> mapDeaths = new HashMap<String, Float>();
        Country c = getCountry(countryName);
        result = (float) c.getTotalDeaths() / (float) c.getTotalConfirmed();
        mapDeaths.put(countryName, result);
        return mapDeaths;
    }

    public HashMap<String, Float> getDeathPercent(){
        float result = -1;
        HashMap<String, Float> MapDeaths = new HashMap<String, Float>();
        var countries = (List<Country>) countryRepository.getCountries();
        for(Country c : countries) {
            result = (float) c.getTotalDeaths() / (float) c.getTotalConfirmed();
            MapDeaths.put(c.getCountry(), result);
        }
        return MapDeaths;
    }

    @Transactional
    public void createCountries(Collection<Country> countries) {
        countryRepository.saveAll(countries);
    }
}
