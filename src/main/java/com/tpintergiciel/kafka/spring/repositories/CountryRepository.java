package com.tpintergiciel.kafka.spring.repositories;

import com.tpintergiciel.kafka.spring.entities.Country;
import com.tpintergiciel.kafka.spring.entities.Global;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CountryRepository extends CrudRepository<Country, Integer> {

    @Query(value = "SELECT * FROM countries c WHERE c.country = :country ORDER BY date DESC LIMIT 192",
    nativeQuery = true)
    Country getCountryValues(@Param("country") String country);

    @Query(value = "SELECT * FROM countries ORDER BY date DESC LIMIT 192",
    nativeQuery = true)
    List<Country> getCountries();



}
