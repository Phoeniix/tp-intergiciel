package com.tpintergiciel.kafka.spring.repositories;


import com.tpintergiciel.kafka.spring.entities.Global;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GlobalRepository extends CrudRepository<Global, Integer> {
    @Query(value = "SELECT * from public.\"global\" ORDER BY date DESC LIMIT 1",
            nativeQuery = true)
    Global getGlobalValues();

    @Query(value = "SELECT global.Total_Confirmed FROM global ORDER BY date DESC LIMIT 1",
            nativeQuery = true)
    Double getTotalConfirmed();

    @Query(value = "SELECT global.Total_Deaths FROM global ORDER BY date DESC LIMIT 1",
            nativeQuery = true)
    Double getTotalDeaths();

    @Query(value = "SELECT global.Total_Recovered FROM global ORDER BY date DESC LIMIT 1",
            nativeQuery = true)
    Double getTotalRecovered();
}
