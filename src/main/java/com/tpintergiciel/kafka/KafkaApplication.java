package com.tpintergiciel.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.springframework.web.util.UriComponentsBuilder.fromHttpUrl;

@SpringBootApplication
public class KafkaApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(KafkaApplication.class, args);
	}

	@Autowired
	private RestTemplate restTemplate;

	private static final ScheduledExecutorService scheduler =
			Executors.newScheduledThreadPool(1);

	@Override
	public void run(String... args) {
		String link = "http://localhost:8080";
		String cmd;
		Scanner sc = new Scanner(System.in);
		HttpEntity<String> requestEntity = new HttpEntity<>(new HttpHeaders());
		final Runnable beeper = () -> {
			URI uri = fromHttpUrl(link).path("/summary")
					.build()
					.encode()
					.toUri();


			restTemplate.exchange(uri, HttpMethod.GET, requestEntity, String.class);
		};
		final ScheduledFuture<?> beeperHandle =
				scheduler.scheduleAtFixedRate(beeper, 0, 60 * 30, SECONDS);
		scheduler.schedule(() -> { beeperHandle.cancel(true); }, 60 * 120, SECONDS);

		while(true) {
			System.out.println("Entrez une commande");
			cmd = sc.nextLine();
			if (cmd.contains(" ")) {
				String[] commands = cmd.split(" ");
				URI requestUriWithCountry = fromHttpUrl(link).path("/req_param")
						.queryParam("cmd", commands[0])
						.queryParam("country", commands[1])
						.build()
						.encode()
						.toUri();
				restTemplate.exchange(requestUriWithCountry, HttpMethod.POST, requestEntity, String.class);
			}
			else {
				URI requestUri = fromHttpUrl(link).path("/req")
						.queryParam("cmd", cmd)
						.build()
						.encode()
						.toUri();
				restTemplate.exchange(requestUri, HttpMethod.POST, requestEntity, String.class);
			}
		}
	}
}
