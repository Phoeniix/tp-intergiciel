package com.tpintergiciel.kafka.kafka.controllers;

import com.tpintergiciel.kafka.kafka.producers.Producer2;
import com.tpintergiciel.kafka.kafka.producers.Producer3;
import com.tpintergiciel.kafka.spring.entities.Country;
import com.tpintergiciel.kafka.spring.entities.Global;
import com.tpintergiciel.kafka.spring.services.CountryService;
import com.tpintergiciel.kafka.spring.services.GlobalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

@RestController
public class MainController {

    private Producer2 producer2;
    private Producer3 producer3;

    @Autowired
    private GlobalService globalService;
    @Autowired
    private CountryService countryService;


    @Autowired
    public MainController(Producer2 producer2, Producer3 producer3) {
        this.producer2 = producer2;
        this.producer3 = producer3;
    }

    @PostMapping("/req")
    public void messageToTopic2(@RequestParam("cmd") String cmd){
        this.producer2.sendMessage(cmd);
    }

    @PostMapping("/req_param")
    public void messageToTopic2WithCountry(@RequestParam("cmd") String cmd, @RequestParam("country") String country) {
        this.producer2.sendMessage(cmd + " " + country);
    }

    @GetMapping("/get_global_values")
    public void getGlobalValuesToTopic3() {
        Global global = globalService.getGlobal();
        this.producer3.sendMessage(global.toString());
    }

    @PostMapping("/get_country_values")
    public void getCountryValuesToTopic3(@RequestParam("country") String country) {
        Country country1 = countryService.getCountry(country);
        this.producer3.sendMessage(country1.toString());
    }

    @GetMapping("/get_confirmed_avg")
    public void getConfirmedAvgToTopic3() {
        String msg = "\nAverage confirmed : " + globalService.averageConfirmed();
        this.producer3.sendMessage(msg);
    }

    @GetMapping("/get_deaths_avg")
    public void getDeathsAvgToTopic3() {
        String msg = "\nAverage deaths : " + globalService.averageDeaths();
        this.producer3.sendMessage(msg);
    }

    @GetMapping("/get_recovered_avg")
    public void getRecoveredAvgToTopic3() {
        String msg = "\nAverage recovered : " + globalService.averageRecovered();
        this.producer3.sendMessage(msg);
    }

    @GetMapping("/get_countries_deaths_percent")
    public void getCountriesDeathsPercentToTopic3() {
        HashMap<String, Float> hmap = countryService.getDeathPercent();
        String message = " --- DEATH PERCENTAGE BY COUNTRY ---\n";
        Map<String, Float> tmap = hmap.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> newValue, TreeMap::new));
        for(Map.Entry<String, Float> entry : tmap.entrySet()) {
            message += "Death percentage in " + entry.getKey() + " : " + entry.getValue() + "%.\n";
        }
        this.producer3.sendMessage(message);
    }

    @PostMapping("/get_countries_deaths_percent")
    public void getCountriesDeathsPercentToTopic3(@RequestParam("country") String country) {
        HashMap<String, Float> map = countryService.getDeathPercent(country);
        StringBuilder message = new StringBuilder();
        for(Map.Entry<String, Float> entry : map.entrySet()) {
            message.append("Death percentage in ").append(entry.getKey()).append(" : ").append(entry.getValue()).append("%.\n");
        }
        this.producer3.sendMessage(message.toString());
    }
}
