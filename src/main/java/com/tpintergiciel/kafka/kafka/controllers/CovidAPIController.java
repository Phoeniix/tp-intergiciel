package com.tpintergiciel.kafka.kafka.controllers;

import com.tpintergiciel.kafka.kafka.producers.Producer1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Objects;

import static org.springframework.web.util.UriComponentsBuilder.fromHttpUrl;

@RestController
public class CovidAPIController {

    private Producer1 producer;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    public CovidAPIController(Producer1 producer) { this.producer = producer;}

    /**
     * Demande les infos à l'api
     * Envoie au consumer1 les infos
     */
    @GetMapping("/summary")
    public void summaryToTopic1() {
        String link = "https://api.covid19api.com";
        String path = "/summary";
        String msg;
        URI uri = fromHttpUrl(link).path(path)
                .build()
                .encode()
                .toUri();
        HttpEntity<String> requestEntity = new HttpEntity<>(new HttpHeaders());
        ResponseEntity responseEntity = restTemplate.exchange(uri, HttpMethod.GET, requestEntity, String.class);
        msg = Objects.requireNonNull(responseEntity.getBody()).toString();
        this.producer.sendMessage(msg);
    }
}
