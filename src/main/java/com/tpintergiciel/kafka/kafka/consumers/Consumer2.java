package com.tpintergiciel.kafka.kafka.consumers;

import com.tpintergiciel.kafka.spring.services.CountryService;
import com.tpintergiciel.kafka.spring.services.GlobalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

import static org.springframework.web.util.UriComponentsBuilder.fromHttpUrl;


@Service
public class Consumer2 {

    @Autowired
    private CountryService countryService;
    @Autowired
    private GlobalService globalService;
    @Autowired
    private RestTemplate restTemplate;

    @KafkaListener(topics = "topic2", groupId = "group_id")
    public void consumeMessage(String cmd) {
        String link = "http://localhost:8080/";
        String userCommand[] = cmd.split(" ");
        URI getUri = fromHttpUrl(link).path(cmd.toLowerCase())
                .build()
                .encode()
                .toUri();

        HttpEntity<String> requestEntity = new HttpEntity<>(new HttpHeaders());

        if (userCommand.length == 2) {
            if (!userCommand[1].isEmpty()) {
                userCommand[1] = userCommand[1].toLowerCase();
            }
        }

        switch (userCommand[0].toLowerCase()) {

            case "get_global_values":
            case "get_confirmed_avg":
            case "get_deaths_avg":
            case "get_recovered_avg":
                restTemplate.exchange(getUri, HttpMethod.GET, requestEntity, String.class);
                break;
            case "get_country_values":
                if (userCommand.length == 2) {
                    userCommand[1] = userCommand[1].substring(0, 1).toUpperCase() + userCommand[1].substring(1);
                    URI getCountryUri = fromHttpUrl(link).path(userCommand[0])
                            .queryParam("country", userCommand[1])
                            .build()
                            .encode()
                            .toUri();
                    restTemplate.exchange(getCountryUri, HttpMethod.POST, requestEntity, String.class);
                } else System.out.println("Veuillez entrer un pays");
                break;
            case "get_countries_deaths_percent":
                if (userCommand.length == 2) {
                    URI getCountryUri = fromHttpUrl(link).path(userCommand[0])
                            .queryParam("country", userCommand[1])
                            .build()
                            .encode()
                            .toUri();
                    restTemplate.exchange(getCountryUri, HttpMethod.POST, requestEntity, String.class);
                } else {
                    restTemplate.exchange(getUri, HttpMethod.GET, requestEntity, String.class);
                }
                break;
            case "help":
                System.out.println(" -- COMMANDES -- :\n" +
                        "- get_global_values : retourne les valeurs globales clés Global du fichier json\n" +
                        "- get_country_values v_pays : retourne les valeurs du pays demandé où v_pays est une chaine de\n" +
                        "caractère du pays demandé\n" +
                        "- get_confirmed_avg : retourne une moyenne des cas confirmés sum(pays)/nb(pays)\n" +
                        "- get_deaths_avg : retourne une moyenne des Décès sum(pays)/nb(pays)\n" +
                        "- get_recovered_avg : retourne une moyenne des soignés(pays)/nb(pays)\n" +
                        "- get_countries_deaths_percent : retourne le pourcentage de Décès par rapport aux cas confirmés\n" +
                        "- get_countries_deaths_percent v_pays : retourne le pourcentage de Décès par rapport aux cas confirmés\n" +
                        "où v_pays est une chaine de caractère du pays demandé\n");
                break;
            default:
                System.out.println("Cette commande n'existe pas");
                break;
        }
    }
}
