package com.tpintergiciel.kafka.kafka.consumers;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class Consumer3 {

    @KafkaListener(topics = "topic3", groupId = "group_id")
    public void consumeMessage(String msg) {
        System.out.println(msg);
    }

}
