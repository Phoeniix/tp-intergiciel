package com.tpintergiciel.kafka.kafka.consumers;


import com.google.gson.Gson;
import com.tpintergiciel.kafka.spring.entities.Global;
import com.tpintergiciel.kafka.spring.entities.Response;
import com.tpintergiciel.kafka.spring.services.CountryService;
import com.tpintergiciel.kafka.spring.services.GlobalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;


@Service
@Transactional
public class Consumer1 {

    @Autowired
    private CountryService countryService;
    @Autowired
    private GlobalService globalService;

    @KafkaListener(topics = "topic1", groupId = "group_id")
    public void consumeMessage(String msg) {
        Gson gson = new Gson();
        Response json = gson.fromJson(msg, Response.class);
        Global global = new Global();
        global.setDate(json.getDate());
        global.setNewConfirmed(json.getGlobal().getNewConfirmed());
        global.setNewDeaths(json.getGlobal().getNewDeaths());
        global.setNewRecovered(json.getGlobal().getNewRecovered());
        global.setTotalConfirmed(json.getGlobal().getTotalConfirmed());
        global.setTotalDeaths(json.getGlobal().getTotalDeaths());
        global.setTotalRecovered(json.getGlobal().getTotalRecovered());
        globalService.createGlobal(global);
        countryService.createCountries(json.getCountries());
    }


}
